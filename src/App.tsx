import { useEffect, useRef, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import './App.css'
import SearchBox from './components/SearchBox'
import TableMemberList, { RefType } from './components/TableMemberList'
import useModal from './function/useModal'
import Modal from './components/Modal'

function App() {
  const members = useRef<any[]>([])
  const refTable = useRef<RefType>(null)
  const fetchData = async () => {
    // เนื่องจาก api ที่ให้มาในแบบทอสอบใช้งานไม่ได้ ตอนนี้เลยทำการ mock data list ขึ้นมาแทน
    let items = [
      {
          engname: 'Mila Kunis',
          thainame: 'มิล่า คูนิส',
          nickname: 'Nick',
          birthday: '1999/08/08',
          height: '178',
          livein: 'กรุงเทพ',
          img: 'https://bootdey.com/img/Content/avatar/avatar1.png'
      },
      {
          engname: 'Poo Panda',
          thainame: 'ปู แพนด้า',
          nickname: 'ปุ้ย',
          birthday: '1966/01/08',
          height: '168',
          livein: 'เชียงใหม่',
          img: 'https://bootdey.com/img/Content/avatar/avatar2.png'
      },
      {
          engname: 'Mike Promp',
          thainame: 'ไมค์ พร้อม',
          nickname: 'แอน',
          birthday: '1988/12/08',
          height: '158',
          livein: 'สงขลา',
          img: 'https://bootdey.com/img/Content/avatar/avatar3.png'
      }
    ];
    members.current = items;
    if(refTable.current) {
      refTable.current.assignItemsValue(items);
    }
  }

  const handleSearchBoxChange = (textValue: string, liveinValue: string, orderBy: string) => {
    let result = handleSearchLiveInChange(members.current, liveinValue);
    result = handleSearchTextChange(result, textValue);
    result = handleSortData(result, orderBy);
    if(refTable.current) {
      refTable.current.assignItemsValue(result);
    }
  }
 

  const handleSearchTextChange = (listData: any[], value: string) => {
    const result = listData.filter(x => x.engname.includes(value) || x.thainame.includes(value) || x.height.includes(value))
    return result;
  }

  const handleSearchLiveInChange = (listData: any[],value: string) => {
    let result = [];
    if (value === 'กรุงเทพ') {
      result = listData.filter(x => x.livein === 'กรุงเทพ')
    }
    else if(value === 'อื่นๆ') {
      result = listData.filter(x => x.livein !== 'กรุงเทพ')
    } else {
      result = listData;
    }
    return result;
  }
  
  const handleSortData = (listData: any[],sortBy: string) => {
    const result = [...listData].sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : ((b[sortBy] > a[sortBy]) ? -1 : 0))
    return result;
  }

  const handleRowClick = async (item: any) => {
    // เนื่องจาก api ที่ให้มาในแบบทอสอบใช้งานไม่ได้ ตอนนี้เลยทำการ mock data ให้คล้ายๆ การไปดึงข้อมูลมาเพิ่ม จาก ข้อมูลที่ได้จาก list ตั้งต้น
    const fetchDetailData = {...item, position: 'พนักงาน', favcolor: 'ทอง'};
    setCurrentItem(fetchDetailData);
    toggle();
  }

  const { isOpen, toggle } = useModal();
  const [currentItem, setCurrentItem] = useState<any>({});
 

  useEffect(() => {
    fetchData()
  }, [])
  return (
    <>
      <div className="container">
        <h1>Member List</h1>
        <SearchBox onSearchBoxChange={ handleSearchBoxChange } />
      </div>
      <TableMemberList ref={ refTable } handleClick={handleRowClick} />
      <Modal isOpen={isOpen} toggle={toggle}>
        <div>
          <img src={currentItem.img} className="rounded mx-auto d-block" alt="..." />
          <div className='row mt-3'>
            <div className='col text-end'>ชื่อ-สกุล: </div>
            <div className='col'>{currentItem.thainame}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>Fullname: </div>
            <div className='col'>{currentItem.engname}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>ชื่อเล่น: </div>
            <div className='col'>{currentItem.nickname}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>วันเกิด: </div>
            <div className='col'>{currentItem.birthday}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>ส่วนสูง: </div>
            <div className='col'>{currentItem.height}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>ที่อยู่: </div>
            <div className='col'>{currentItem.livein}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>ตำแหน่งงาน: </div>
            <div className='col'>{currentItem.position}</div>
          </div>
          <div className='row'>
            <div className='col text-end'>สีที่ชอบ: </div>
            <div className='col'>{currentItem.favcolor}</div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default App
