import React, { ReactNode } from "react";

interface ModalType {
  children?: ReactNode;
  isOpen: boolean;
  toggle: () => void;
}

export default function Modal(props: ModalType) {
  return (
    <>
      {props.isOpen && (
        <div className="modal modal-overlay " onClick={props.toggle}>
          <div onClick={(e) => e.stopPropagation()} className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-body">
                    {props.children}
                </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
