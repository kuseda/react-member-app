import React, { ChangeEvent, useEffect, useRef, useState } from 'react'
import useDebounce from '../function/useDebounce'

interface Props {
    onSearchBoxChange: (textValue: string, liveinValue: string, orderBy: string) => void
}
const SearchBox = ({onSearchBoxChange}: Props) => {
    const [value, setValue] = useState<string>('')
    const [livein, setLivein] = useState<string>('all')
    const [sortby, setSortBy] = useState<string>('engname')
    const debouncedValue = useDebounce<string>(value, 500)

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value)
    }

    const onValueChange = (event: ChangeEvent<HTMLInputElement>) => {
        let radioVaule: string = event.target.value;
        console.log(radioVaule);
        let searchLiveIn = radioVaule === 'all' ? '' : radioVaule === 'กรุงเทพ' ? 'กรุงเทพ' : 'อื่นๆ';
        setLivein(radioVaule);
        onSearchBoxChange(debouncedValue, searchLiveIn, sortby);
    }

    const onSelectValueChange = (event: ChangeEvent<HTMLSelectElement>) => {
        let dropdownValue: string = event.target.value;
        setSortBy(dropdownValue);
        onSearchBoxChange(debouncedValue, livein, dropdownValue);
    }
    
    useEffect(() => {
        onSearchBoxChange(debouncedValue, livein, sortby);
    }, [debouncedValue])
  return (
    <>
    <div className="row g-3">
        <div className="col-auto pt-1">
            <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" checked={livein === "all"} onChange={ onValueChange } value={'all'} />
                <label className="form-check-label" htmlFor="inlineRadio1">ทั้งหมด</label>
            </div>
            <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" checked={livein === "กรุงเทพ"} onChange={ onValueChange } value={'กรุงเทพ'} />
                <label className="form-check-label" htmlFor="inlineRadio2">กรุงเทพ</label>
            </div>
            <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" checked={livein === "อื่นๆ"} onChange={ onValueChange } value={'อื่นๆ'} />
                <label className="form-check-label" htmlFor="inlineRadio3">ต่างจังหวัด</label>
            </div>
        </div>
        <div className="col-auto">
            <input type="text" value={value} onChange={handleChange} className='form-control' /> 
        </div>
        <div className="col-auto text-right">
            <label className="col-form-label"> Sort by:</label>
        </div>
        <div className="col-auto">
            <select className="form-select" aria-label="Default select example" onChange={ onSelectValueChange } value={sortby}>
                <option value="engname" >ชื่อนามสกุล</option>
                <option value="nickname">ชื่อเล่น</option>
                <option value="birthday">วันเกิด</option>
                <option value="height">ส่วนสูง</option>
            </select>
        </div>
    </div>
    </>
  )
}

export default SearchBox