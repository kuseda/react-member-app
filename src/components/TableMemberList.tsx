import React, { forwardRef, useImperativeHandle, useState } from 'react'

interface Props {
    handleClick: (item: any) => void
}
export interface RefType {
    assignItemsValue: (values: any[]) => void;
}
const TableMemberList = ({ handleClick }: Props, ref: React.Ref<RefType>) => {
    const [items, setItems] = useState<any[]>([])
    useImperativeHandle(ref, () => ({
        assignItemsValue(values: any[]) {
            setItems(values);
        }
    }));   
      
  return (
    <>
        <div className="container">
            <div className="row">
                <div className="col col-lg-12">
                    <div className="main-box clearfix">
                        <div className="table-responsive">
                        <table className="table user-list">
                            <thead>
                            <tr>
                                <th><span>Member</span></th>
                                <th><span>BirthDay</span></th>
                                <th className="text-center"><span>Height</span></th>
                                <th><span>Live In</span></th>
                            </tr>
                            </thead>
                            { 
                            items && items.length === 0  && 
                                <tbody>
                                    <tr>
                                        <td colSpan={4}>Data NotFound.</td>
                                    </tr>
                                </tbody>
                            }
                            { 
                            items && items.length > 0  && 
                                <tbody>
                                { items.map((x, index) => 
                                    <tr key={ index } onClick={()=> handleClick(x)}>
                                        <td>
                                            <img src={ x.img } alt="" />
                                            <span className="user-link">{ x.engname }</span>
                                            <span className="user-subhead">{ x.thainame }</span>
                                            <span className="user-subhead">{ x.nickname }</span>
                                        </td>
                                        <td>
                                            { x.birthday }
                                        </td>
                                        <td className="text-center">
                                            { x.height } cm
                                        </td>
                                        <td>
                                            { x.livein }
                                        </td>
                                    </tr>
                                )}
                                </tbody> 
                            }
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </>
  )
}

export default forwardRef(TableMemberList)